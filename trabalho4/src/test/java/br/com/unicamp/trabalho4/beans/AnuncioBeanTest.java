package br.com.unicamp.trabalho4.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class AnuncioBeanTest {

	@Test
	void testGetValor_getValorZero_success() {
		AnuncioBean anuncio = new AnuncioBean();
		
		assertEquals(0.0, anuncio.getValor());
	}
	
	@Test
	void testGetValor_getValorSemDesconto_success() {
		AnuncioBean anuncio = new AnuncioBean();
		ProdutoBean produto = new ProdutoBean();
		produto.setValor(10.0);
		anuncio.setProduto(produto);
		
		assertEquals(10.0, anuncio.getValor());
	}
	
	@Test
	void testGetValor_getValorComDesconto_success() {
		AnuncioBean anuncio = new AnuncioBean();
		ProdutoBean produto = new ProdutoBean();
		produto.setValor(10.0);
		anuncio.setDesconto(0.1);
		anuncio.setProduto(produto);
		
		assertEquals(9.0, anuncio.getValor());
	}
	
	@Test
	void testGetValor_getValorComDescontoNegativo_success() {
		AnuncioBean anuncio = new AnuncioBean();
		ProdutoBean produto = new ProdutoBean();
		produto.setValor(10.0);
		anuncio.setDesconto(-0.1);
		anuncio.setProduto(produto);
		
		assertEquals(11.0, anuncio.getValor());
	}
	
	@Test
	void testGetValor_getValorNull_success() {
		AnuncioBean anuncio = new AnuncioBean();
		ProdutoBean produto = new ProdutoBean();
		produto.setValor(null);
		anuncio.setProduto(produto);
		
		assertThrows(NullPointerException.class, anuncio::getValor);
	}
	
	@Test
	void testGetValor_getValorDescontoNull_success() {
		AnuncioBean anuncio = new AnuncioBean();
		ProdutoBean produto = new ProdutoBean();
		produto.setValor(10.0);
		anuncio.setDesconto(null);
		anuncio.setProduto(produto);
		
		assertThrows(NullPointerException.class, anuncio::getValor);
	}

}
