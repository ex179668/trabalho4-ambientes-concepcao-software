package br.com.unicamp.trabalho4.beans;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AnuncianteBeanTest {

	@Test
	void testAddAnuncio_addAnuncio_success() {
		AnuncianteBean anunciante = new AnuncianteBean();
		AnuncioBean anuncio = new AnuncioBean();
		anunciante.addAnuncio(anuncio);
		
		assertEquals(1, anunciante.getAnuncios().size());
		assertEquals(anuncio, anunciante.getAnuncios().get(0));
	}

	@Test
	void testRemoveAnuncio_removeFirstItem_success() {
		AnuncianteBean anunciante = new AnuncianteBean();
		AnuncioBean anuncio = new AnuncioBean();
		anunciante.addAnuncio(anuncio);
		
		assertDoesNotThrow(() -> anunciante.removeAnuncio(0));
		assertEquals(0, anunciante.getAnuncios().size());
	}
	
	@Test
	void testRemoveAnuncio_removeFirstItem_throwExcption() {
		AnuncianteBean anunciante = new AnuncianteBean();
		
		assertThrows(IndexOutOfBoundsException.class, () -> anunciante.removeAnuncio(0));
	}

	@Test
	void testValorMedioAnuncios_caculoMedia_NaN() {
		AnuncianteBean anunciante = new AnuncianteBean();
		
		assertEquals(Double.NaN, anunciante.valorMedioAnuncios());
	}
	
	@Test
	void testValorMedioAnuncios_caculoMedia_success() {
		AnuncianteBean anunciante = new AnuncianteBean();
		anunciante.addAnuncio(getNewAnuncioWithPrice(10.0));
		anunciante.addAnuncio(getNewAnuncioWithPrice(10.0));
		anunciante.addAnuncio(getNewAnuncioWithPrice(10.0));
		
		assertEquals(10.0, anunciante.valorMedioAnuncios());
	}

	private AnuncioBean getNewAnuncioWithPrice(Double valor) {
		AnuncioBean anuncio = new AnuncioBean();
		anuncio.setProduto(new ProdutoBean(null, null, null, valor, null));
		
		return anuncio;
	}

}
