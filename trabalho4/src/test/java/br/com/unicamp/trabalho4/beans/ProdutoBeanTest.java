package br.com.unicamp.trabalho4.beans;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ProdutoBeanTest {

	@Test
	void testCompareTo_compare_equals() {
		ProdutoBean produto1 = new ProdutoBean();
		ProdutoBean produto2 = new ProdutoBean();
		
		produto1.setValor(10.0);
		produto2.setValor(10.0);
		
		assertEquals(0, produto1.compareTo(produto2));
	}
	
	@Test
	void testCompareTo_compare_less() {
		ProdutoBean produto1 = new ProdutoBean();
		ProdutoBean produto2 = new ProdutoBean();
		
		produto1.setValor(10.0);
		produto2.setValor(11.0);
		
		assertEquals(-1, produto1.compareTo(produto2));
	}
	
	@Test
	void testCompareTo_compare_greater() {
		ProdutoBean produto1 = new ProdutoBean();
		ProdutoBean produto2 = new ProdutoBean();
		
		produto1.setValor(10.0);
		produto2.setValor(9.0);
		
		assertEquals(1, produto1.compareTo(produto2));
	}
	
	@Test
	void testCompareTo_compareValorNull_exception() {
		ProdutoBean produto1 = new ProdutoBean();
		ProdutoBean produto2 = new ProdutoBean();
		
		produto1.setValor(null);
		produto2.setValor(11.0);
		
		assertThrows(NullPointerException.class, () -> produto1.compareTo(produto2));
	}
	
	@Test
	void testCompareTo_compareOtherProdutoValorNull_exception() {
		ProdutoBean produto1 = new ProdutoBean();
		ProdutoBean produto2 = new ProdutoBean();
		
		produto1.setValor(10.00);
		produto2.setValor(null);
		
		assertThrows(NullPointerException.class, () -> produto1.compareTo(produto2));
	}

}
